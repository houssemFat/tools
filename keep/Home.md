Welcome to the lmg-espace-adherent-front-end-pwa wiki!

# LMG-espace-adherent-front-end-pwa
L'application client (front + PWA ) de l'Espace adhérant pour LMG (La mutuelle générale) 

# Code conventions 

## Typescript 

  - Angular 6 + angular/pwa
  - We follow a majority of (palantir Lint rules)[https://palantir.github.io/tslint/]
  - We use also (codelyzer)[http://codelyzer.com/] for static code analysis 

## Sass/Css 

  - We use [BEM](http://getbem.com/) convention for class names Block Element Modifier

## Svg Files and icons 
  - All icons should be in `assets\icons` without sub folder 
  - For each icon, define or use classess in `_icons` (no url link in the html)
  - If no svg is provided, use a temproray png file with this name pattern `icon-name.tmp.png`, avoid using the `:after` , 'content' ou `img` (no url src in the url), use the class defined in the `_icon` class with a div, i or span

--- 

  - Toutes les icônes doivent être dans `assets\icons`  (directement sans dossier parent) et pas dans `assets\images\icons` ou autres.
 - Les png sont temporaires, si vous utilisez du png, véfifier bien que vous  mettez `nom.tmp.png` pour qu'on la remplacera ensuite par un svg
- Pour chaque icône *de l'application*, éviter de mettre :after 'content' ou img (pas d'url dans le html), il faut définir une classe dans le fichier _icons et l'utiliser avec un i ou div ou span

## Test 

  - Write test for all components/modules/helpers/services/
  - Code coverage for each file should be equal or greater to 50% of total file 

## Accessiblity 

  - We should respect the maximum of bronze criteria of [WCAG 2.0](https://www.w3.org/TR/WCAG20/)

## PWA 
 - A guide for PWA development application [google codelab](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/)
 - Use the [web server for chrome](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb?hl=en) or serve it by your won server 

## Git flow Gitkraken 

- Feature par functionality 
- Each developer should rebase its work to develop when develop move 
- Code review is mandatory before each merge 
- Test for each feature 

### Jenkins 

Jenkins is running on our [local server](http://172.16.31.40:10000/blue/organizations/jenkins/LMG-espace-adherent-front-end-pwa/activity), we use blue ocean to test/build and package the application locally. __Note__ that we should use TFS tools in the LMG side.  


# LmgEspaceAdherantFrontEndPwa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.0.

## Development server

Run `npm run dev` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
