Conditions de validations de PR : 


### Code 

- Validation de Tslint 
- Ne plus accepter du Grosse PR (sauf exception)
- Nomenclature et commentaires 

### Test

- Les tests unitaires sont obligatoires pour chaque nouvelle partie de code (composant, service, module, classe , model ..... )
- Essayez de vérifier le test avec coverage. 
- Un test ou plus pour chaque méthode ajoutée

### Lighthouse 

- La vérification par lighthouse du chrome des pages html, accessibilités, warning ... est à faire, il faut corriger le maximum des points possible

- Internet explorer doit être vérifier avant le merge

### Build 

- ng build 
- Tester le build avec un serveur (http-server, python simpleHttp ...... ou apache)
- Tourner sur votre pc 

### Jenkins 

- jenkins doit être mis à jour 


