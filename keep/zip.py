#!/usr/bin/env python3
# python 3.3
import datetime
import shutil
import tempfile
import os
from distutils.dir_util import copy_tree


with tempfile.TemporaryDirectory() as tmpdirname:
    archive_name = "build-"  + datetime.date.today().strftime("%d-%m-%y")
    copy_tree('build', tmpdirname)
    shutil.make_archive(archive_name, 'zip', tmpdirname)
