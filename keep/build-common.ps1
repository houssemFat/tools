$build_dist = "build"
$build_assets_dist = "$Env:BUILD_SOURCESDIRECTORY\$build_dist\assets"
$src_code = "$Env:BUILD_SOURCESDIRECTORY\OIDC_SOURCE\src\common"

function copy_folder($folder)
{
    $dist_folder = "$build_assets_dist\$folder"
    
    if( !(Test-Path $dist_folder) )
    {
        mkdir $dist_folder
    }
    
    Copy-Item "$src_code\assets\$folder\*" -Destination "$dist_folder\"
}

### Generate CSS file from specific SCSS files
node-sass $Env:BUILD_SOURCESDIRECTORY\OIDC_SOURCE\src\common\scss\style.scss -o $build_assets_dist --output-style compressed

### Copy of all folders
copy_folder("fonts")
copy_folder("icons")
copy_folder("images")

Copy-Item $src_code\assets\main.js $build_assets_dist\main.js

Copy-Item $src_code\favicon.ico $Env:BUILD_SOURCESDIRECTORY\$build_dist\favicon.ico
Copy-Item $src_code\index.html $Env:BUILD_SOURCESDIRECTORY\$build_dist\index.html
Copy-Item $src_code\assets\errors.json $Env:BUILD_SOURCESDIRECTORY\$build_dist\errors.json

Write-Host "Generating common resources done"