$clientID = @("35be5ec0-b9ab-4972-b793-b0e1c509ddf9", "019fd3f9-5487-4c2e-8d8d-89c10241b7b5", "066e0672-4b28-422d-aa44-ef42d0aad64a", "f25b5279-d346-4ffb-8686-5bd224dfa39e", "2fd61538-78e8-4047-88af-2db0183a380f", "2c6f83eb-2af2-419e-a37b-cfabf35a824a")

function GenerateClient {
    
    foreach($client in $clientID)
    {
        $dist = "$Env:BUILD_SOURCESDIRECTORY\build\$client"

        Write-Host "Generate ressources for $client"

        if( !(Test-Path $dist) )
        {
            mkdir $dist
        }

         ### Copy JS files
        Copy-Item $Env:BUILD_SOURCESDIRECTORY\OIDC_SOURCE\src\client\main.js $dist

        ### Generate CSS file from specific SCSS files
        node-sass $Env:BUILD_SOURCESDIRECTORY\OIDC_SOURCE\src\client\style.scss -o $dist --output-style compressed
    }
}

GenerateClient