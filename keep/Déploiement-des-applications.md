Once you checkedout a branch you need to follow this instruction :


* `npm install` : install newly added packages since last modification


To generate a build version for : 


##### DEV 

* `npm run build-dev`     (equivalent to run `ng build -c homol`)

##### HOMOL 
 
* `npm run build-homol`


#### !! PRODUCTION     
* `npm run build --prod`    

 


