#!/usr/bin/env bash

BUILD_DIST="build"
BUILD_ASSETS_DIST="${BUILD_DIST}/assets"
SRC_CODE="src/common"

copy_folder(){
   FOLDER=$1
   DIST_FOLDER="$BUILD_ASSETS_DIST/$FOLDER"
   if [ ! -d "$DIST_FOLDER" ]; then
    mkdir  ${DIST_FOLDER}
    fi
    cp ${SRC_CODE}/assets/${FOLDER}/* ${DIST_FOLDER}/

}
# generate sass for
node-sass  ${SRC_CODE}/scss/style.scss -o ${BUILD_ASSETS_DIST} --output-style compressed

# copy all fonts, images and icons folder

copy_folder fonts
copy_folder icons
copy_folder images


cp ${SRC_CODE}/assets/main.js ${BUILD_ASSETS_DIST}/main.js


cp ${SRC_CODE}/favicon.ico ${BUILD_DIST}/favicon.ico
cp ${SRC_CODE}/index.html ${BUILD_DIST}/index.html

cp ${SRC_CODE}/assets/errors.json ${BUILD_DIST}/errors.json

echo "Generating common resources done"