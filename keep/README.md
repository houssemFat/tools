# LMG-espace-adherent-oidc-login
L'interface de connection avec l'OIDC de l'espace adhérent pour LMG (La mutuelle générale)

Steps to build a version :

requirements :

Sass compiler : used to build scss file , for this code we node-sass

### Bash scripts :

`build-common.sh` generate common resources

 - Generate the css files from src/common/sass folder
 - copy all common assets
 - copy index.html
 - copy favicon
 - copy errors



`build-client.sh [client-1] [client-2] [client-3] [...]` generate client resources
`build-client.sh a294cc71-863f-4181-a57e-9252f0877036 145513b5-a3b8-4b70-af3a-faaadf364352 019fd3f9-5487-4c2e-8d8d-89c10241b7b5 35be5ec0-b9ab-4972-b793-b0e1c509ddf9` generate client resources

 - Generate the css file of the given client id scss file ( `src/client/main.scss` file to `/build/{client-id}/main.css` )
 - Copy `src/client/main.js` file to `/build/{client-id}/main.js`


 List of clients for LMG

 - https://lmg-demo.herokuapp.com : `a294cc71-863f-4181-a57e-9252f0877036`
 - localhost:4200 : `145513b5-a3b8-4b70-af3a-faaadf364352`
 - homol  : `019fd3f9-5487-4c2e-8d8d-89c10241b7b5`
 - dev : `35be5ec0-b9ab-4972-b793-b0e1c509ddf9`
