#!/usr/bin/env bash



generate_client(){
   CLIENT=$1
   DIST="build/${CLIENT}/"
   echo "Generate ressource for $CLIENT "

   if [ ! -d "$DIST" ]; then
      mkdir  ${DIST}
   fi
   # copy js files
   cp  src/client/main.js ${DIST}
   # generate css file from specific sass files
   node-sass  src/client/style.scss -o  ${DIST} --output-style compressed
}

if [[ $# -eq 0 ]] ; then
        echo '[warning], no client id passed, please specify at least one client '
        echo 'use for example : ./build-client.sh a294cc71-863f-4181-a57e-9252f0877036 145513b5-a3b8-4b70-af3a-faaadf364352 019fd3f9-5487-4c2e-8d8d-89c10241b7b5'
    else
        if [ ! -d "build" ]; then
          mkdir  "build"
       fi
        for var in "$@"
        do
            generate_client $var
        done
fi

#
