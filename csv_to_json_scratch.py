# file to format keys
# given a csv file with keys and langages , it will generate a json file for each lang in the csv format
# line 0 nop
# locales is in line 2
# translation starts from line 3

## format of file

"""

BLABLA
Keys,	English corrected,	Deutsch: corrected version,	Svenska (Corrected)
header/logout,	Log out,	Abmelden,	Logga ut
header/switch/stock,	Stock,	Bestand  ( Gaslager ),	Lager
......

"""

from os import path
import json

# list of locales
locales = []
# contains each dictionary for each locales
"""
# {
      locale_1 : { ..... },
      locale_2 : { ..... }
    }
"""
dict = {}


def clean(item):
  item = item.replace('\n', '')
  return item


def add_key(keys_as_array, locale, value):
  if not dict.get(locale):
    dict[locale] = {}
  _dict = dict[locale]
  for (index, _key) in enumerate(keys_as_array):
    if index == len(keys_as_array) - 1:
      _dict[_key] = clean(value)
    else:
      if _dict.get(_key) is None:
        _dict[_key] = {}

      _dict = _dict[_key]


def format_entry(line):
  key = line.split(',')[0]
  translations = line.split(',')[1:]
  keys = key.split('/')
  keys = [clean(item) for item in keys]
  for (index, locale) in enumerate(locales):
    add_key(keys, locale, translations[index])


with open(path.join('.', 'Qlixbi - Manage - Multilingue V1 copy - Sheet1.csv'), 'r', encoding="utf-8") as r:
  lines = r.readlines()
  if len(lines) > 3:
    locales = lines[1].split(',')[1:]
    # clean locales
    locales = [clean(item) for item in locales]
    locales = [item.replace(':', ' ') for item in locales]
    # prepare dictionaries
    for (index, line) in enumerate(lines[2:]):
      format_entry(line)
    """
    with open('all.json', 'w',  encoding="utf-8") as file:
        print("writing all file ")
        file.write(str(dict))
    """
    # write to json files
    for locale in locales:
      print(locale)
      with open(locale + '.json', 'w', encoding="utf-8") as file:
        print("writing " + locale + ".json file ")
        file.write(json.dumps(dict[locale], ensure_ascii=False))

  else:
    raise IOError("A csv file is required")
